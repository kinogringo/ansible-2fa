# auth_google_role

Role for hardening of SSH service with Google Authenticator as a second factor. Role allows to create/delete/edit users and their ssh public keys and disable 2fa for them.

### Prerequisites

* Host machines with SSH
* Control machine with installed ansible and its dependencies

## Usage


### Deploy

1. Fill inventory.yml according to these rules:
```YAML
pam_servers:
  hosts:
    server1: 							# hostname
      ansible_host: 192.168.43.220     	# ip address
      ansible_ssh_user: provision		# connection user with sudo rights (or root)
      ansible_ssh_pass: prov  			# connection password (optional if sshd configured with publickey authentication)
      users:							# list with user information TO CURRENT ROLE EXECUTION
        - name: lol						# name [neccessary] 
          pub_key_path: /home/provision/Desktop/ansible_test1/lol.pub # key path on master node [optional]
          google_auth: yes				# enable/disable 2fa [optional, default yes]
          extra_action: 				# extra actions to ansible user module
            state: present				# present(create/edit) or absent(delete) [neccecary if it's needed to edit user]
            # arguments [option] to edit users. Will be edited if set. E.g. user 'lol' will be updated with new groups, password and shell
            groups: []					
            password: $6$tTnxBnzgAx4csB1h$ncWZ.POWZks4X0mcWQKmsZWCs6itjq5HpfvoMnuujwESpKs8Fy/SJ3ivLhm6t0XgxjzOfNHNtBSGaedYK2njW1
            shell: /bin/bash

        # EXAMPLE to delete user
        # - name: kek
        #   extra_action: 
        #     state: absent
        #     remove: yes

```
1. Create playbook. `become: true` is neccecary
1. Run playbook. E.g. `ansible-playbook -i inventory.yml test.yml`

